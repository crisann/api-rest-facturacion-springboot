package com.apifacturacion.rest.interfaceservice;

import java.util.List;

import  com.apifacturacion.rest.modelo.Colaborador;

public interface IcolaboradorService {
	List<Colaborador> listar();
	
	void create(Colaborador colaborador);

    void edit(Colaborador colaborador);

    void remove(Integer colaborador);
    
    String ingresar(String colaborador);
}
