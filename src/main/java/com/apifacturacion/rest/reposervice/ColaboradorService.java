package com.apifacturacion.rest.reposervice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.apifacturacion.rest.interfaceservice.IcolaboradorService;
import com.apifacturacion.rest.modelo.Colaborador;
import com.apifacturacion.rest.repositorio.ColaboradorRepo;

@Service
public class ColaboradorService implements IcolaboradorService {

	@Autowired
	private ColaboradorRepo colaboradorRepo;
	
	@Override
	public List<Colaborador> listar() {
		// TODO Auto-generated method stub
		return (List<Colaborador>) colaboradorRepo.findAll();
	}

	@Override
	public void create(Colaborador colaborador) {
		// TODO Auto-generated method stub
		colaboradorRepo.save(colaborador);
		System.out.println("El colaborador se registro correctamente.");
		
	}

	@Override
	public void edit(Colaborador colaborador) {
		colaboradorRepo.save(colaborador);
		System.out.println("El colaborador se modifico correctamente.");
		
	}

	@Override
	public void remove(Integer colaborador) {
		// TODO Auto-generated method stub
		colaboradorRepo.deleteById(colaborador);
		System.out.println("El colaborador se elimno correctamente");
		
	}

	@Override
	public String ingresar(String colaborador){
		// TODO Auto-generated method stub
		
		Colaborador obColaborador = colaboradorRepo.findByNombreuser(colaborador);
		
		if(obColaborador == null) {
			System.out.println("Usuario no registrado o incorrecto");
		}

		return obColaborador.getNombreuser();
	}

}
